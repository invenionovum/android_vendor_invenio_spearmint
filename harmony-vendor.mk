# Copyright (C) 2018 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is generated by device/invenio/spearmint/setup-makefiles.sh

PRODUCT_COPY_FILES += \
    vendor/invenio/spearmint/proprietary/vendor/bin/akmd09911:$(TARGET_COPY_OUT_VENDOR)/bin/akmd09911 \
    vendor/invenio/spearmint/proprietary/vendor/bin/akmd8963:$(TARGET_COPY_OUT_VENDOR)/bin/akmd8963 \
    vendor/invenio/spearmint/proprietary/vendor/bin/akmd8975:$(TARGET_COPY_OUT_VENDOR)/bin/akmd8975 \
    vendor/invenio/spearmint/proprietary/vendor/bin/ami304d:$(TARGET_COPY_OUT_VENDOR)/bin/ami304d \
    vendor/invenio/spearmint/proprietary/vendor/bin/bmm050d:$(TARGET_COPY_OUT_VENDOR)/bin/bmm050d \
    vendor/invenio/spearmint/proprietary/vendor/bin/ccci_fsd:$(TARGET_COPY_OUT_VENDOR)/bin/ccci_fsd \
    vendor/invenio/spearmint/proprietary/vendor/bin/ccci_mdinit:$(TARGET_COPY_OUT_VENDOR)/bin/ccci_mdinit \
    vendor/invenio/spearmint/proprietary/vendor/bin/fuelgauged:$(TARGET_COPY_OUT_VENDOR)/bin/fuelgauged \
    vendor/invenio/spearmint/proprietary/vendor/bin/geomagneticd:$(TARGET_COPY_OUT_VENDOR)/bin/geomagneticd \
    vendor/invenio/spearmint/proprietary/vendor/bin/gsm0710muxd:$(TARGET_COPY_OUT_VENDOR)/bin/gsm0710muxd \
    vendor/invenio/spearmint/proprietary/vendor/bin/gsm0710muxdmd2:$(TARGET_COPY_OUT_VENDOR)/bin/gsm0710muxdmd2 \
    vendor/invenio/spearmint/proprietary/vendor/bin/mc6420d:$(TARGET_COPY_OUT_VENDOR)/bin/mc6420d \
    vendor/invenio/spearmint/proprietary/vendor/bin/md_ctrl:$(TARGET_COPY_OUT_VENDOR)/bin/md_ctrl \
    vendor/invenio/spearmint/proprietary/vendor/bin/memsicd:$(TARGET_COPY_OUT_VENDOR)/bin/memsicd \
    vendor/invenio/spearmint/proprietary/vendor/bin/memsicd3416x:$(TARGET_COPY_OUT_VENDOR)/bin/memsicd3416x \
    vendor/invenio/spearmint/proprietary/vendor/bin/msensord:$(TARGET_COPY_OUT_VENDOR)/bin/msensord \
    vendor/invenio/spearmint/proprietary/vendor/bin/muxreport:$(TARGET_COPY_OUT_VENDOR)/bin/muxreport \
    vendor/invenio/spearmint/proprietary/vendor/bin/nvram_agent_binder:$(TARGET_COPY_OUT_VENDOR)/bin/nvram_agent_binder \
    vendor/invenio/spearmint/proprietary/vendor/bin/nvram_daemon:$(TARGET_COPY_OUT_VENDOR)/bin/nvram_daemon \
    vendor/invenio/spearmint/proprietary/vendor/bin/permission_check:$(TARGET_COPY_OUT_VENDOR)/bin/permission_check \
    vendor/invenio/spearmint/proprietary/vendor/bin/pq:$(TARGET_COPY_OUT_VENDOR)/bin/pq \
    vendor/invenio/spearmint/proprietary/vendor/bin/s62xd:$(TARGET_COPY_OUT_VENDOR)/bin/s62xd \
    vendor/invenio/spearmint/proprietary/vendor/bin/sn:$(TARGET_COPY_OUT_VENDOR)/bin/sn \
    vendor/invenio/spearmint/proprietary/vendor/bin/spm_loader:$(TARGET_COPY_OUT_VENDOR)/bin/spm_loader \
    vendor/invenio/spearmint/proprietary/vendor/bin/terservice:$(TARGET_COPY_OUT_VENDOR)/bin/terservice \
    vendor/invenio/spearmint/proprietary/vendor/bin/thermal_manager:$(TARGET_COPY_OUT_VENDOR)/bin/thermal_manager \
    vendor/invenio/spearmint/proprietary/vendor/bin/wmt_launcher:$(TARGET_COPY_OUT_VENDOR)/bin/wmt_launcher \
    vendor/invenio/spearmint/proprietary/vendor/bin/wmt_loader:$(TARGET_COPY_OUT_VENDOR)/bin/wmt_loader \
    vendor/invenio/spearmint/proprietary/vendor/etc/.tp/.ht120.mtc:$(TARGET_COPY_OUT_VENDOR)/etc/.tp/.ht120.mtc \
    vendor/invenio/spearmint/proprietary/vendor/etc/.tp/thermal.conf:$(TARGET_COPY_OUT_VENDOR)/etc/.tp/thermal.conf \
    vendor/invenio/spearmint/proprietary/vendor/etc/.tp/thermal.off.conf:$(TARGET_COPY_OUT_VENDOR)/etc/.tp/thermal.off.conf \
    vendor/invenio/spearmint/proprietary/vendor/etc/audio_effects.conf:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.conf \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/conn.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/conn.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/emsvr.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/emsvr.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/fuelgauged.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/fuelgauged.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/mnld.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/mnld.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/mtk_agpsd.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/mtk_agpsd.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/nvram_daemon.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/nvram_daemon.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/pq.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/pq.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/sensor.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/sensor.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/spm.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/spm.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/thermal_manager.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/thermal_manager.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/wifi2agps.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/wifi2agps.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/init/wpa_supplicant.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/wpa_supplicant.rc \
    vendor/invenio/spearmint/proprietary/vendor/etc/mtk_omx_core.cfg:$(TARGET_COPY_OUT_VENDOR)/etc/mtk_omx_core.cfg \
    vendor/invenio/spearmint/proprietary/vendor/etc/seccomp_policy/mediacodec.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy \
    vendor/invenio/spearmint/proprietary/vendor/etc/wifi/p2p_supplicant_overlay.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/p2p_supplicant_overlay.conf \
    vendor/invenio/spearmint/proprietary/vendor/etc/wifi/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf \
    vendor/invenio/spearmint/proprietary/vendor/etc/wifi/wpa_supplicant_overlay.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant_overlay.conf \
    vendor/invenio/spearmint/proprietary/vendor/firmware/config.ini:$(TARGET_COPY_OUT_VENDOR)/firmware/config.ini \
    vendor/invenio/spearmint/proprietary/vendor/firmware/pcm_deepidle.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/pcm_deepidle.bin \
    vendor/invenio/spearmint/proprietary/vendor/firmware/pcm_sodi.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/pcm_sodi.bin \
    vendor/invenio/spearmint/proprietary/vendor/firmware/pcm_suspend.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/pcm_suspend.bin \
    vendor/invenio/spearmint/proprietary/vendor/firmware/ram_patch.fw:$(TARGET_COPY_OUT_VENDOR)/firmware/ram_patch.fw \
    vendor/invenio/spearmint/proprietary/vendor/firmware/ROMv2_lm_patch_1_0_hdr.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/ROMv2_lm_patch_1_0_hdr.bin \
    vendor/invenio/spearmint/proprietary/vendor/firmware/ROMv2_lm_patch_1_1_hdr.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/ROMv2_lm_patch_1_1_hdr.bin \
    vendor/invenio/spearmint/proprietary/vendor/firmware/WIFI_RAM_CODE_6580:$(TARGET_COPY_OUT_VENDOR)/firmware/WIFI_RAM_CODE_6580 \
    vendor/invenio/spearmint/proprietary/vendor/firmware/WMT_SOC.cfg:$(TARGET_COPY_OUT_VENDOR)/firmware/WMT_SOC.cfg \
    vendor/invenio/spearmint/proprietary/vendor/firmware/catcher_filter_1_wg_n.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/catcher_filter_1_wg_n.bin \
    vendor/invenio/spearmint/proprietary/vendor/firmware/modem_1_wg_n.img:$(TARGET_COPY_OUT_VENDOR)/firmware/modem_1_wg_n.img \
    vendor/invenio/spearmint/proprietary/vendor/lib/egl/egl.cfg:$(TARGET_COPY_OUT_VENDOR)/lib/egl/egl.cfg \
    vendor/invenio/spearmint/proprietary/vendor/lib/egl/libGLES_mali.so:$(TARGET_COPY_OUT_VENDOR)/lib/egl/libGLES_mali.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/hw/audio.primary.mt6580.so:$(TARGET_COPY_OUT_VENDOR)/lib/hw/audio.primary.mt6580.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/hw/camera.mt6580.so:$(TARGET_COPY_OUT_VENDOR)/lib/hw/camera.mt6580.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/hw/gralloc.mt6580.so:$(TARGET_COPY_OUT_VENDOR)/lib/hw/gralloc.mt6580.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/hw/hwcomposer.mt6580.so:$(TARGET_COPY_OUT_VENDOR)/lib/hw/hwcomposer.mt6580.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/hw/memtrack.mt6580.so:$(TARGET_COPY_OUT_VENDOR)/lib/hw/memtrack.mt6580.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/hw/sensors.vendor.mt6580.so:$(TARGET_COPY_OUT_VENDOR)/lib/hw/sensors.vendor.mt6580.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/lib3a.so:$(TARGET_COPY_OUT_VENDOR)/lib/lib3a.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libc2kutils.so:$(TARGET_COPY_OUT_VENDOR)/lib/libc2kutils.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libClearMotionFW.so:$(TARGET_COPY_OUT_VENDOR)/lib/libClearMotionFW.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libJpgDecPipe.so:$(TARGET_COPY_OUT_VENDOR)/lib/libJpgDecPipe.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libJpgEncPipe.so:$(TARGET_COPY_OUT_VENDOR)/lib/libJpgEncPipe.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxAdpcmDec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxAdpcmDec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxAdpcmEnc.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxAdpcmEnc.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxApeDec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxApeDec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxCore.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxCore.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxFlacDec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxFlacDec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxG711Dec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxG711Dec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxGsmDec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxGsmDec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxMp3Dec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxMp3Dec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxRawDec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxRawDec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxVdecEx.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxVdecEx.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxVenc.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxVenc.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libMtkOmxVorbisEnc.so:$(TARGET_COPY_OUT_VENDOR)/lib/libMtkOmxVorbisEnc.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libSwJpgCodec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libSwJpgCodec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libaed.so:$(TARGET_COPY_OUT_VENDOR)/lib/libaed.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libaudiocompensationfilter.so:$(TARGET_COPY_OUT_VENDOR)/lib/libaudiocompensationfilter.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libaudiocomponentengine.so:$(TARGET_COPY_OUT_VENDOR)/lib/libaudiocomponentengine.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libaudiocustparam.so:$(TARGET_COPY_OUT_VENDOR)/lib/libaudiocustparam.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libaudiodcrflt.so:$(TARGET_COPY_OUT_VENDOR)/lib/libaudiodcrflt.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libaudiosetting.so:$(TARGET_COPY_OUT_VENDOR)/lib/libaudiosetting.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libaudiotoolkit.so:$(TARGET_COPY_OUT_VENDOR)/lib/libaudiotoolkit.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libbessound_hd_mtk.so:$(TARGET_COPY_OUT_VENDOR)/lib/libbessound_hd_mtk.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libblisrc.so:$(TARGET_COPY_OUT_VENDOR)/lib/libblisrc.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libblisrc32.so:$(TARGET_COPY_OUT_VENDOR)/lib/libblisrc32.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libbluetooth_mtk.so:$(TARGET_COPY_OUT_VENDOR)/lib/libbluetooth_mtk.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libbt-vendor.so:$(TARGET_COPY_OUT_VENDOR)/lib/libbt-vendor.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libbwc.so:$(TARGET_COPY_OUT_VENDOR)/lib/libbwc.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.camadapter.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.camadapter.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.campipe.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.campipe.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.camshot.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.camshot.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.client.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.client.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.device1.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.device1.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.device3.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.device3.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.exif.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.exif.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.exif.v3.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.exif.v3.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.hal3a.v3.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.hal3a.v3.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.halsensor.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.halsensor.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.iopipe.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.iopipe.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.iopipe_FrmB.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.iopipe_FrmB.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.metadata.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.metadata.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.metadataprovider.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.metadataprovider.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.paramsmgr.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.paramsmgr.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.utils.sensorlistener.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.utils.sensorlistener.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam.utils.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam.utils.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam1_utils.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam1_utils.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam3_app.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam3_app.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam3_hwnode.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam3_hwnode.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam3_hwpipeline.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam3_hwpipeline.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam3_pipeline.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam3_pipeline.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam3_utils.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam3_utils.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam_hwutils.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam_hwutils.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam_mmp.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam_mmp.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam_platform.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam_platform.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcam_utils.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcam_utils.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcamalgo.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcamalgo.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcamdrv.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcamdrv.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcamdrv_FrmB.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcamdrv_FrmB.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcameracustom.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcameracustom.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcustom_nvram.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcustom_nvram.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcustom_prop.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcustom_prop.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libcvsd_mtk.so:$(TARGET_COPY_OUT_VENDOR)/lib/libcvsd_mtk.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libdpframework.so:$(TARGET_COPY_OUT_VENDOR)/lib/libdpframework.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libdrmmtkutil.so:$(TARGET_COPY_OUT_VENDOR)/lib/libdrmmtkutil.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libdrmmtkwhitelist.so:$(TARGET_COPY_OUT_VENDOR)/lib/libdrmmtkwhitelist.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libfeatureio.so:$(TARGET_COPY_OUT_VENDOR)/lib/libfeatureio.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libfgauge.so:$(TARGET_COPY_OUT_VENDOR)/lib/libfgauge.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libfile_op.so:$(TARGET_COPY_OUT_VENDOR)/lib/libfile_op.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libfs_mgr.so:$(TARGET_COPY_OUT_VENDOR)/lib/libfs_mgr.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libgas.so:$(TARGET_COPY_OUT_VENDOR)/lib/libgas.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libged.so:$(TARGET_COPY_OUT_VENDOR)/lib/libged.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libgpu_aux.so:$(TARGET_COPY_OUT_VENDOR)/lib/libgpu_aux.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libgralloc_extra.so:$(TARGET_COPY_OUT_VENDOR)/lib/libgralloc_extra.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libgui_ext.so:$(TARGET_COPY_OUT_VENDOR)/lib/libgui_ext.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libh264dec_customize.so:$(TARGET_COPY_OUT_VENDOR)/lib/libh264dec_customize.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libh264dec_sd.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libh264dec_sd.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libh264dec_se.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libh264dec_se.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libh264dec_xa.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libh264dec_xa.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libh264dec_xb.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libh264dec_xb.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libh264enc_sa.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libh264enc_sa.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libhwm.so:$(TARGET_COPY_OUT_VENDOR)/lib/libhwm.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libimageio.so:$(TARGET_COPY_OUT_VENDOR)/lib/libimageio.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libimageio_plat_drv.so:$(TARGET_COPY_OUT_VENDOR)/lib/libimageio_plat_drv.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libion_mtk.so:$(TARGET_COPY_OUT_VENDOR)/lib/libion_mtk.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libm4u.so:$(TARGET_COPY_OUT_VENDOR)/lib/libm4u.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmhalImageCodec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmhalImageCodec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmmprofile.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmmprofile.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmmsdkservice.feature.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmmsdkservice.feature.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmmsdkservice.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmmsdkservice.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmp4dec_sa.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmp4dec_sa.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmrdump.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmrdump.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmsbc_mtk.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmsbc_mtk.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmtcloader.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmtcloader.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmtk_drvb.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmtk_drvb.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmtk_mmutils.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmtk_mmutils.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmtkjpeg.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmtkjpeg.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmtklimiter.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmtklimiter.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmtkplayer.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmtkplayer.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmtkshifter.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmtkshifter.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libnvram.so:$(TARGET_COPY_OUT_VENDOR)/lib/libnvram.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libnvram_daemon_callback.so:$(TARGET_COPY_OUT_VENDOR)/lib/libnvram_daemon_callback.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libnvram_platform.so:$(TARGET_COPY_OUT_VENDOR)/lib/libnvram_platform.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libnvram_sec.so:$(TARGET_COPY_OUT_VENDOR)/lib/libnvram_sec.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libnvramagentclient.so:$(TARGET_COPY_OUT_VENDOR)/lib/libnvramagentclient.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libperfservicenative.so:$(TARGET_COPY_OUT_VENDOR)/lib/libperfservicenative.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libpq_cust.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpq_cust.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libpq_prot.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpq_prot.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libpqservice.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpqservice.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/librilmtk.so:$(TARGET_COPY_OUT_VENDOR)/lib/librilmtk.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/librilmtkmd2.so:$(TARGET_COPY_OUT_VENDOR)/lib/librilmtkmd2.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/librrc.so:$(TARGET_COPY_OUT_VENDOR)/lib/librrc.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libshowlogo.so:$(TARGET_COPY_OUT_VENDOR)/lib/libshowlogo.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libspeech_enh_lib.so:$(TARGET_COPY_OUT_VENDOR)/lib/libspeech_enh_lib.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libstagefrighthw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libstagefrighthw.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libui_ext.so:$(TARGET_COPY_OUT_VENDOR)/lib/libui_ext.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libvc1dec_sa.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libvc1dec_sa.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libvcodec_cap.so:$(TARGET_COPY_OUT_VENDOR)/lib/libvcodec_cap.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libvcodec_oal.so:$(TARGET_COPY_OUT_VENDOR)/lib/libvcodec_oal.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libvcodec_utility.so:$(TARGET_COPY_OUT_VENDOR)/lib/libvcodec_utility.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libvcodecdrv.so:$(TARGET_COPY_OUT_VENDOR)/lib/libvcodecdrv.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libvp8dec_sa.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libvp8dec_sa.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libvp9dec_sa.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libvp9dec_sa.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/mediadrm/libwvdrmengine.so:$(TARGET_COPY_OUT_VENDOR)/lib/mediadrm/libwvdrmengine.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/mtk-ril.so:$(TARGET_COPY_OUT_VENDOR)/lib/mtk-ril.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/mtk-rilmd2.so:$(TARGET_COPY_OUT_VENDOR)/lib/mtk-rilmd2.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/drm/libdrmctaplugin.so:$(TARGET_COPY_OUT_VENDOR)/lib/drm/libdrmctaplugin.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/drm/libdrmmtkplugin.so:$(TARGET_COPY_OUT_VENDOR)/lib/drm/libdrmmtkplugin.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libHEVCdec_sa.ca7.android.so:$(TARGET_COPY_OUT_VENDOR)/lib/libHEVCdec_sa.ca7.android.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmmprofile_jni.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmmprofile_jni.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmp4dec_sb.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmp4dec_sb.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libmp4enc_xa.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libmp4enc_xa.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/libvp8dec_xa.ca7.so:$(TARGET_COPY_OUT_VENDOR)/lib/libvp8dec_xa.ca7.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/mediadrm/libdrmclearkeyplugin.so:$(TARGET_COPY_OUT_VENDOR)/lib/mediadrm/libdrmclearkeyplugin.so \
    vendor/invenio/spearmint/proprietary/vendor/lib/mediadrm/libmockdrmcryptoplugin.so:$(TARGET_COPY_OUT_VENDOR)/lib/mediadrm/libmockdrmcryptoplugin.so
